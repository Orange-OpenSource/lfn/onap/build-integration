#!/bin/bash
# SPDX-license-identifier: Apache-2.0

set -o errexit
set -o nounset
set -o pipefail

echo "Clone xtesting repository"
git clone "${XTESTING_GIT}"
cd xtesting/


CI_REGISTRY_IMAGE_TAG=${GERRIT_REVIEW}-${GERRIT_PATCHSET}
PYTHONSDK_TESTS_GERRIT_CHANGE_TAG="refs/changes/${GERRIT_REVIEW: -2}/${GERRIT_REVIEW}/${GERRIT_PATCHSET}"

echo "Build pythonsdk-tests image with gerrit patchset's pythonsdk-tests version"
cd smoke-usecases-pythonsdk/
docker build --build-arg ONAP_TESTS_TAG=$PYTHONSDK_TESTS_GERRIT_CHANGE_TAG -t $CI_REGISTRY_IMAGE:$CI_REGISTRY_IMAGE_TAG -f ./docker/Dockerfile .
docker build --build-arg ONAP_TESTS_TAG=$PYTHONSDK_TESTS_GERRIT_CHANGE_TAG -t $CI_REGISTRY_IMAGE:$CI_REGISTRY_IMAGE_TAG-smoke -f ./docker/Dockerfile .
cd ..

echo "Build infra-helthcheck image with gerrit patchset's pythonsdk-tests version"
cd infra-healthcheck/
docker build --build-arg ONAP_TESTS_TAG=$PYTHONSDK_TESTS_GERRIT_CHANGE_TAG -t $CI_REGISTRY_IMAGE:$CI_REGISTRY_IMAGE_TAG-infra-helthcheck -f ./docker/Dockerfile .


echo "Push an image into registry"
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker push $CI_REGISTRY_IMAGE:$CI_REGISTRY_IMAGE_TAG
docker push $CI_REGISTRY_IMAGE:$CI_REGISTRY_IMAGE_TAG-smoke
docker push $CI_REGISTRY_IMAGE:$CI_REGISTRY_IMAGE_TAG-infra-helthcheck
